@extends('app')

@section('pageTitle')

Photo

@stop

@section('pageClass') quiz-page @stop

@section('content')

<div class="row">

  <div class="col-md-6">
    <div class="row">
      <div class="col-md-6 chart chartTop">
        <canvas id="jpChart" width="150" height="150"></canvas>
        <div class="chartLabel">
          <span class="chartLabelLeftValue">JUDGING</span>
          <span class="chartLabelRightValue">PERCEIVING</span>
        </div>
      </div>
      <div class="col-md-6 chart chartTop">
        <canvas id="ftChart" width="150" height="150"></canvas>
        <div class="chartLabel">
          <span class="chartLabelLeftValue">FEELING</span>
          <span class="chartLabelRightValue">THINKING</span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 chart">
        <canvas id="ieChart" width="150" height="150"></canvas>
        <div class="chartLabel">
          <span class="chartLabelLeftValue">INTROVERSION</span>
          <span class="chartLabelRightValue">EXTROVERSION</span>
        </div>
      </div>
      <div class="col-md-6 chart">
        <canvas id="snChart" width="150" height="150"></canvas>
        <div class="chartLabel">
          <span class="chartLabelLeftValue">SENSING</span>
          <span class="chartLabelRightValue">INTUITION</span>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6 quiz">
    <h3 class="first">Question @{{ questionNumber }} of 32</h3>
    Select the option that most closely describes yourself.<br><br>
    <div class="trait">@{{ question.trait1 }}</div>
    <br>
    <button type="button" class="btn btn-default btn-large btn-quiz" v-on:click.stop="handleAnswer(1)" >usually @{{question.trait1}}</button>
    <br>
    <button type="button" class="btn btn-default btn-large btn-quiz" v-on:click.stop="handleAnswer(2)" >sometimes @{{question.trait1}}</button>
    <br>
    <button type="button" class="btn btn-default btn-large btn-quiz" v-on:click.stop="handleAnswer(3)" >neither</button>
    <br>
    <button type="button" class="btn btn-default btn-large btn-quiz" v-on:click.stop="handleAnswer(4)" >sometimes @{{question.trait2}}</button>
    <br>
    <button type="button" class="btn btn-default btn-large btn-quiz" v-on:click.stop="handleAnswer(5)" >usually @{{question.trait2}}</button>
    <br><br>
    <div class="trait">@{{ question.trait2 }}</div>
    <br>
    <br>
    Results:<br><br>
    IE - @{{results.IE}}<br>
    SN - @{{results.SN}}<br>
    FT - @{{results.FT}}<br>
    JP - @{{results.JP}}<br><br>

    Scores:<br><br>
    IE - @{{scores.IE}}<br>
    SN - @{{scores.SN}}<br>
    FT - @{{scores.FT}}<br>
    JP - @{{scores.JP}}<br><br>
  </div>

</div>

@stop
